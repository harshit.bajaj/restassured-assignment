package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;

import io.restassured.RestAssured;

public class PutRequest {
	@Test(priority = 1)
	public void putRequest() {

		JSONObject request = new JSONObject();
		request.put("name", "Harshit");
		request.put("job", "QA");
		System.out.println(request);
		RestAssured.baseURI = "https://reqres.in";
		given().baseUri(baseURI).body(request.toJSONString()).when().put("/api/users/2").then().statusCode(200).log().all().body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:55:58.772Z"));
	}
}
