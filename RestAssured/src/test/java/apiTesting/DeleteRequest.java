package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class DeleteRequest {
	@Test(priority = 1)
	public void deleteRequest() {
		RestAssured.baseURI = "https://reqres.in";
		given().baseUri(baseURI).when().delete("/api/users/2").then().statusCode(204).log().all();
	}
}
