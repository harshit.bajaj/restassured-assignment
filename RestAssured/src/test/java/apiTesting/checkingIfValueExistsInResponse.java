package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class checkingIfValueExistsInResponse {
	@Test(priority = 1)
	public void getMethodSingleValue() {
		RestAssured.baseURI = "https://reqres.in";
		given().baseUri(baseURI).when().get("/api/users?page=2").then().body("data.first_name",hasItem("Byron"));
	}
	
	@Test(priority = 2)
	public void getMethodMultipleValue() {
		RestAssured.baseURI = "https://reqres.in";
		given().baseUri(baseURI).when().get("/api/users?page=2").then().body("data.first_name",hasItems("Byron", "Rachel")).body("data.last_name",hasItems("Lawson"));
	}
}

//hasItem is used to check if single value exists in the response and
//hasItems is used to check multiple value existance in response