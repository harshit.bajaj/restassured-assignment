package apiTesting;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

public class PostRequest {
	@Test(priority = 1)
	public void postRequest() {

		JSONObject request = new JSONObject();
		request.put("name", "Harshit");
		request.put("job", "QA");
		System.out.println(request);
		RestAssured.baseURI = "https://reqres.in";
		given().baseUri(baseURI).body(request.toJSONString()).when().post("/api/users").then().statusCode(201);
	}
}
