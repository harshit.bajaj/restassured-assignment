package apiTesting;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import static io.restassured.RestAssured.*;

public class GetRequest {
  @Test(priority = 1)
  public void firstGetMethod() {
	 Response res =  RestAssured.get("https://reqres.in/api/users?page=2");
	 int statusCode = res.statusCode();
	 System.out.println(statusCode);
	 String resBody = res.body().asString();
	 System.out.println(resBody);
  }
  
  @Test(priority = 2)
  public void assertGetMethod() {
	 Response res =  RestAssured.get("https://reqres.in/api/users?page=2");
	 int statusCode = res.statusCode();
	 Assert.assertEquals(200, statusCode);
  }
  

  @Test(priority = 3)
  public void getMethodUsingGivenWhenThen() {
	RestAssured.baseURI = "https://reqres.in";
	Response res = given().baseUri(baseURI).when().get("/api/users?page=2").then().statusCode(201).extract().response();
	String resString = res.asString();
	JsonPath js = new JsonPath(resString);
	System.out.println(js.getString("data[0].email").equalsIgnoreCase("michael.lawson@reqres.in"));
	Assert.assertEquals(js.getString("data[0].email").equalsIgnoreCase("michael.lawson@reqres.in"), true);
	
	res.then().log().all();
  }
}
