package Assignment;

import org.json.simple.JSONObject;
import io.restassured.parsing.Parser;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.baseURI;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class GoRestRequests {
	String accessToken ="Bearer a032ff2b81e1e8ab1f8fc6e2907f88646f16c30ec2c2499fc630615530f174b7";
	String id = "";
	JSONObject req = new JSONObject();
	String baseURI = "https://gorest.co.in";
	@Test(priority = 2)
	public void getMethod() {
		System.out.println("GET REQUEST");
		String res =  given().baseUri(baseURI).header("Authorization",accessToken).when().get("/public/v2/users/"+ id).then().extract().response().asString();
		System.out.println(res);

	}

	@Test(priority = 1)
	public void postMethod() {
		System.out.println("POST REQUEST");
		req.put("name", "Harshit Bajaj");
		req.put("email", "ww2ef@gmail.com");
		req.put("gender", "male");
		req.put("status", "active");
		
		String res = given().baseUri(baseURI).headers("Authorization",accessToken,"Content-Type","application/json")
				.body(req.toJSONString())
				.when().post("/public/v2/users")
				.then().extract().response().asString();
		JsonPath jsp = new JsonPath(res);
		id = jsp.getString("id");
		System.out.println(res);
		req.clear();
	}

	@Test(priority = 3)
	public void putMethod() {
		System.out.println("PUT REQUEST");
		req.put("name", "Harshit Kajaj");
		req.put("email", "dcs@gmail.com");
		req.put("gender", "male");
		req.put("status", "active");
		
		String resPut = given().baseUri(baseURI).header("Authorization",accessToken)
				.body(req.toJSONString())
				.when().put("/public/v2/users/"+id)
				.then().extract().response().asString();
		System.out.println(resPut);
		req.clear();
	}

	@Test(priority = 4)
	public void deleteMethod() {
		System.out.println("DELETE REQUEST");
		given().baseUri(baseURI).header("Authorization",accessToken).when().delete("/public/v2/users/"+id).then().extract().response().asString();
	}
}
